package test;

	import java.io.IOException;
	import javax.servlet.http.*;
	import java.io.BufferedReader;
	import java.io.InputStream;
	import java.io.InputStreamReader;
	import java.net.URL;
	import java.util.ArrayList;

	@SuppressWarnings("serial")
	public class Show extends HttpServlet {
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException {
			if (req.getParameter("message") == null) {
			      resp.setContentType("text/plain");
			      resp.getWriter().println("No found parameter to for other servlet. \n\n");
			    } 
			else{ 
				ArrayList<String> array = new ArrayList<String>();
			    array.add("http://step-homework-hnoda.appspot.com/");
			    array.add("http://step-test-krispop.appspot.com/");
			    array.add("http://yuki-stephw7.appspot.com/");
			    array.add("http://1-dot-step-homework-kitade.appspot.com/");
			    
			   resp.setContentType("text/plain");
			   resp.setCharacterEncoding("UTF-8");
		      resp.getWriter().println("["+req.getParameter("message")+"]");
		      for(int i=0;i<array.size();i++){
		    	  StringBuilder urlstr = new StringBuilder(array.get(i));
		    	  urlstr.append("convert?message="+req.getParameter("message"));
		    	  URL url = new URL(urlstr.toString());
		      
		    	  InputStream input = url.openStream();
		    	  BufferedReader reader = new BufferedReader(new InputStreamReader(input,"UTF-8"));
		    	  String line;
		    	  StringBuilder xmlres = new StringBuilder();
		    	  while ((line = reader.readLine()) != null) {
		    		  xmlres.append(line);
		    	  }
		    	  reader.close();
		    	  String st = xmlres.toString();
		    	  resp.getWriter().println("url:"+array.get(i));
		    	  resp.getWriter().println("	"+st);
		      	}
		     }
		}
	}
Yuko Yanagawa
